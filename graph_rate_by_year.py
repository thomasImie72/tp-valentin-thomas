import matplotlib.pyplot as plt
from functions import *


data = {
    '1996': getAverageMovieRateByYear(1996),
    '2000': getAverageMovieRateByYear(2000),
    '2010': getAverageMovieRateByYear(2010),
    '2015': getAverageMovieRateByYear(2015)
}
names = list(data.keys())
values = list(data.values())

fig, axs = plt.subplots()
axs.bar(names, values)

fig.suptitle('Moyenne des notes par année.')

plt.show()
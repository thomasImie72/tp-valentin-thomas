import csv, sqlite3

conn = sqlite3.connect('movies.db')

c = conn.cursor()

# Drop tables
# c.execute('DROP table movies')
# c.execute('DROP table ratings')
# c.execute('DROP table tags')
#
# # Create table movies
# c.execute('''CREATE TABLE movies (movie_id integer PRIMARY KEY, title text, genres text)''')
#
# # Create table rates
# c.execute('''CREATE TABLE ratings (id integer PRIMARY KEY AUTOINCREMENT, user_id integer, rates_movie_id integer, rating integer, timestamp integer,
#             FOREIGN KEY(rates_movie_id) REFERENCES movies(movie_id))''')
#
# Create table tags
# c.execute('''CREATE TABLE tags (id integer PRIMARY KEY AUTOINCREMENT, user_id integer, tags_movie_id integer, tag text, timestamp integer,
#             FOREIGN KEY(tags_movie_id) REFERENCES movies(movie_id))''')

#
# # ________________
# # Insert from csv file MOVIES
# with open('movies.csv','r', encoding='utf-8') as fin: # `with` statement available in 2.5+
#     # csv.DictReader uses first line in file for column headings by default
#     dr = csv.DictReader(fin) # comma is default delimiter
#     to_db = [(i['movieId'], i['title'], i['genres']) for i in dr]
#
# c.executemany("INSERT INTO movies (movie_id, title, genres) VALUES (?, ?, ?);", to_db)
# ________________

# ________________
# Insert from csv file RATING
# with open('ratings500.csv','r') as fin: # `with` statement available in 2.5+
#     # csv.DictReader uses first line in file for column headings by default
#     dr = csv.DictReader(fin) # comma is default delimiter
#     to_db = [(i['userId'], i['movieId'], i['rating'], i['timestamp']) for i in dr]
#
# c.executemany("INSERT INTO ratings (user_id, rates_movie_id, rating, timestamp) VALUES (?, ?, ?, ?);", to_db)
# ________________

# ________________
# Insert from csv file TAGS
# with open('tags.csv','r', encoding='utf-8') as fin: # `with` statement available in 2.5+
#     # csv.DictReader uses first line in file for column headings by default
#     dr = csv.DictReader(fin) # comma is default delimiter
#     to_db = [(i['userId'], i['movieId'], i['tag'], i['timestamp']) for i in dr]
#
# c.executemany("INSERT INTO tags (user_id, tags_movie_id, tag, timestamp) VALUES (?, ?, ?, ?);", to_db)
# ________________

# Select * from table movies
# c.execute('SELECT * FROM movies')
# list = c.fetchall()
# for i in list:
#     print(i)
# print('LENGTH :', len(list))

# Select * from table ratings
# c.execute('SELECT * FROM ratings')
# list = c.fetchall()
# for i in list:
#     print(i)
# print('LENGTH :', len(list))

# Select * from table tags
# c.execute('SELECT * FROM tags')
# list = c.fetchall()
# for i in list:
#     print(i)
# print('LENGTH :', len(list))

# Save (commit) the changes
conn.commit()

# We can also close the connection if we are done with it.
# Just be sure any changes have been committed or they will be lost.
conn.close()
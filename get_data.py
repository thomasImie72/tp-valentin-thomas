import requests
from requests.packages.urllib3 import add_stderr_logger
import urllib
from bs4 import BeautifulSoup
from urllib.error import HTTPError
from urllib.request import urlopen
import re, random, datetime
random.seed(datetime.datetime.now())

add_stderr_logger()

url = "http://movielens.org/home"

session = requests.Session()
per_session = session.post(url,
data={'userName':'MrFougere', 'password':'fdqfs'})
#you can now associate request with beautifulsoup
try:
  #it assumed that by now you are logged so we can now use .get and fetch any page of your choice
  bsObj = BeautifulSoup(session.get(url).content, 'html.parser')
except HTTPError as e:
  print(e)


print (bsObj.prettify())

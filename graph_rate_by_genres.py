import matplotlib.pyplot as plt
from functions import *


data = {
    'Documentary': getAverageRateByGenre("Documentary"),
    'Sci-Fi': getAverageRateByGenre("Sci-Fi"),
    'Comedy': getAverageRateByGenre("Comedy"),
    'Thriller': getAverageRateByGenre("Thriller"),
    'Action': getAverageRateByGenre("Action")
}
names = list(data.keys())
values = list(data.values())

fig, axs = plt.subplots()
axs.bar(names, values)

fig.suptitle('Moyenne des notes par genre.')

plt.show()
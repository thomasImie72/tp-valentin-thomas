import sqlite3
from statistics import mean


def getMovieById(id):
    conn = sqlite3.connect('movies.db')
    c = conn.cursor()

    c.execute('Select * FROM movies WHERE movie_id = ' + str(id))
    res = c.fetchall()
    conn.close()
    return res


def getAllMovies():
    conn = sqlite3.connect('movies.db')
    c = conn.cursor()

    c.execute('Select * FROM movies')
    res = c.fetchall()
    conn.close()

    return res

def getAverageMovieRateById(id):
    conn = sqlite3.connect('movies.db')
    c = conn.cursor()

    c.execute('Select rating '
              'FROM ratings '
              'WHERE rates_movie_id = ' + str(id))
    res = c.fetchall()

    if len(res) == 0:
        return -1

    rate_list = []
    for r in res:
        rate_list.append(r[0])
    average = mean(rate_list)

    conn.close()
    return average


def getAverageMovieRateByYear(year):
    conn = sqlite3.connect('movies.db')
    c = conn.cursor()

    c.execute("Select movie_id "
              "FROM movies "
              "WHERE title LIKE '%(" + str(year) + ")'")
    movie_list = c.fetchall()

    rate_list = []
    for m in movie_list:
        # print('...' + str(year) + '...')
        rate = getAverageMovieRateById(m[0])
        if rate is not -1:
            rate_list.append(rate)

    average = -1
    try:
        average = mean(rate_list)
    except Exception as e:
        print('error : ', e)

    conn.close()
    return average

def getAverageRateByGenre(genre):
    conn = sqlite3.connect('movies.db')
    c = conn.cursor()

    c.execute("Select movie_id "
              "FROM movies "
              "WHERE genres LIKE '%" + str(genre) + "'")
    movie_list = c.fetchall()

    rate_list = []
    for m in movie_list:
        # print('...' + str(year) + '...')
        rate = getAverageMovieRateById(m[0])
        if rate is not -1:
            rate_list.append(rate)

    average = -1
    try:
        average = mean(rate_list)
    except Exception as e:
        print('error : ', e)

    conn.close()
    return average

def getAverageRateByTag(tag):
    conn = sqlite3.connect('movies.db')
    c = conn.cursor()

    c.execute("Select tags_movie_id "
              "FROM tags "
              "WHERE tag LIKE '%" + str(tag) + "'")
    movie_list = c.fetchall()

    rate_list = []
    for m in movie_list:
        # print('...' + str(year) + '...')
        rate = getAverageMovieRateById(m[0])
        if rate is not -1:
            rate_list.append(rate)

    average = -1
    try:
        average = mean(rate_list)
    except Exception as e:
        print('error : ', e)

    conn.close()
    return average


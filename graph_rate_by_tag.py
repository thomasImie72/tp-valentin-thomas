import matplotlib.pyplot as plt
from functions import *


data = {
    'jesus': getAverageRateByTag("jesus"),
    'animation': getAverageRateByTag("animation"),
    'Tarantino': getAverageRateByTag("Tarantino"),
}
names = list(data.keys())
values = list(data.values())

fig, axs = plt.subplots()
axs.bar(names, values)

fig.suptitle('Moyenne des notes par tag.')

plt.show()
from surprise.model_selection import cross_validate

import pandas as pd

from surprise import SVD
from surprise import Dataset
from surprise import accuracy
from surprise.model_selection import train_test_split

# Load the movielens-100k dataset (download it if needed),
data = Dataset.load_builtin('ml-100k')

# sample random trainset and testset
# test set is made of 25% of the ratings.
trainset, testset = train_test_split(data, test_size=.25)

# for item in trainset.all_items():
#     print(item)


# We'll use the famous SVD algorithm.
algo = SVD()

# Train the algorithm on the trainset, and predict ratings for the testset
algo.fit(trainset)
predictions = algo.test(testset)

# # Then compute RMSE
# accuracy.rmse(predictions)
def get_Iu(uid):
   """ return the number of items rated by given user
   args:
     uid: the id of the user
   returns:
     the number of items rated by the user
   """
   try:
       return len(trainset.ur[trainset.to_inner_uid(uid)])
   except ValueError:  # user was not part of the trainset
       return 0


def get_Ui(iid):
   """ return number of users that have rated given item
   args:
     iid: the raw id of the item
   returns:
     the number of users that have rated the item.
   """
   try:
       return len(trainset.ir[trainset.to_inner_iid(iid)])
   except ValueError:
       return 0


df = pd.DataFrame(predictions, columns=['uid', 'iid', 'rui', 'est', 'details'])
df['Iu'] = df.uid.apply(get_Iu)
df['Ui'] = df.iid.apply(get_Ui)
df['err'] = abs(df.est - df.rui)
best_predictions = df.sort_values(by='err')[:10]
worst_predictions = df.sort_values(by='err')[-10:]

print(best_predictions)
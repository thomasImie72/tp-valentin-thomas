Travail effectu� : 

- Enregistrement des donn�es dans un BDD SQLITE
(code command� dans le fichier database.py)

- Exploration visuelle des donn�es avec MatplotLib

	lancer les scripts :
		graph_rate_by_genre.py pour afficher les graphiques des notes moyennes par genre de film
		graph_rate_by_tag.py pour afficher les graphiques des notes moyennes par tag
		graph_rate_by_year.py pour afficher les graphiques des notes moyennes par ann�e

- Utilisation de Scikit-learn pour avoir les recommandations de films (en console)

__________________________________________________________________________________________________________

Ce que nous avons appris : 

- manipuler des donn�es en python � partir de fichier en csv
- ajouter des donn�es � une BDD SQLITE
- cr�er des graph sur maplotlib
- utiliser l'outil de recommandation de scikit-learn
- manipuler Orange (bien que les donn�es que nous avions ne nous on pas permis de cr�er de graphique coh�rent)

__________________________________________________________________________________________________________

Ce qui nous a manqu� :

- de l'EXPERIENCE 
- du temps
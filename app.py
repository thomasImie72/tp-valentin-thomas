from flask import Flask, render_template, request

app = Flask(__name__)


@app.route('/', methods=['GET', 'POST'])
def index():

    if request.method == 'POST':
        movie_title = request.form['movie_title']

        print(movie_title)

    else:
        print('error')

    return render_template('index.html')


@app.route('/result')
def result():
    if request.method == 'POST':
        movie_title = request.form['movie_title']

        print('movie_title : ', movie_title)

    else:
        print('erreur')


# an exception

if __name__ == "__main__":
	app.run(debug=True)